# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. puts the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.




require "byebug"

def guessing_game
# debugger
  number = rand(1..100)


  counter = 0
  user_number = nil
  until user_number == number

    puts "Guess a number"
    user_number = gets.chomp.to_i
    puts user_number
    if user_number > number
      puts "too high"
    elsif user_number < number
      puts "too low"
    end
    counter += 1
  end

  puts user_number
  puts counter

end

#* reads that file * shuffles the lines * saves it to the file "{input_name}-shuffled.text".
def file_shuffler
  puts "give me a file name"
  file_name = gets.chomp
  content = File.readlines(file_name)
  f = File.open("#{file_name}-shuffled.text", "w") do |f|
    f.puts content.shuffle
  end
end
